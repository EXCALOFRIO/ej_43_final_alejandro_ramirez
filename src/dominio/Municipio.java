package dominio;
import java.util.ArrayList;
import aplicacion.*;

public class Municipio{
    private String nombre;

    private int numeroDeHabitantes;

    public int getNumeroDeHabitantes() {
        return numeroDeHabitantes;
    }
    public void setNumeroDeHabitantes(int numeroDeHabitantes) {
        this.numeroDeHabitantes = numeroDeHabitantes;
    }

    public ArrayList<Localidad> coleccionLocalidades = new ArrayList<>();


    public String getNombre(){
        return nombre;
    }
    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String toString() {
        return "Municipio [nombre=" + nombre + ", numeroDeHabitantes=" + numeroDeHabitantes + "]";
    }

    public int calcularNumeroHabitantes(){
        for(int i = 0; i<coleccionLocalidades.size();i++) {
            numeroDeHabitantes+=(coleccionLocalidades.get(i)).getNumeroDeHabitantes();
        }
        return  numeroDeHabitantes;

    }

}